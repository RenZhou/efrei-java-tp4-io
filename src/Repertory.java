import java.io.*;

public class Repertory {

    private final String path = "./out/";
    private String nameRepertory;

    public Repertory(String nameRepertory) throws IOException {
        this.nameRepertory = nameRepertory;
    }

    private FileOutputStream getFOS() throws IOException {
        File file = new File(path + nameRepertory);
        if (!file.exists()) {
            file.createNewFile();
        }
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(file, true);
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("fichier introuvable");
        }
        return fos;
    }

    private void writeToFS(FileOutputStream fos) throws IOException {
        System.out.println("Entrez les informations indiques");
        System.out.print("Nom : ");
        String name = MyInOut.myReaderLine();
        System.out.print("Age : ");
        String age = MyInOut.myReaderLine();
        System.out.print("Nb enfant : ");
        String nbChild = MyInOut.myReaderLine();
        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fos));
        out.write(name + ", ");
        out.write(age + ", ");
        out.write(nbChild + "\n");
        out.flush();
        System.out.println("Vous avez entrez : " + name + " " + age + " " + nbChild);
    }

    public void inputInfo() throws IOException {
        FileOutputStream fos = getFOS();
        writeToFS(fos);
        fos.close();
    }

    // M��thode lister : liste sur ��cran le contenu du fichier
    public void lister() throws IOException {
        FileInputStream fis = getFIS();
        BufferedReader in = new BufferedReader(new InputStreamReader(fis));
        String line;
        System.out.println("--------------------- FILE CONTENT ---------------------");
        for (int nbPerso = 0; (line = in.readLine()) != null; ++nbPerso) {
            System.out.println("Fichier numero " + nbPerso);
            String[] separated = line.split(",");
            System.out.println("Nom :        " + separated[0]);
            System.out.println("Age :        " + separated[1]);
            System.out.println("Nb Enfants : " + separated[2] + "\n");
        }
        fis.close();
        System.out.println("--------------------- END OF FILE ---------------------");
    }

    private FileInputStream getFIS() throws FileNotFoundException {
        System.out.println("Veuillez saisir le nom du fichier a lire");
        String fileName = MyInOut.myReaderLine();
        File file = new File(path + fileName);
        if (!file.exists()) {
            throw new FileNotFoundException("Fichier n'est pas trouver");
        }
        return new FileInputStream(file);
    }

    private void showInfo(String info) {
    }

    // M��thode chercherInfo : affiche les informations relatives �� une personne
    // dont le nom est introduit par le clavier
    public void chercherInfo() throws IOException {
        //Le nom de la personne dont on veut afficher les informations est lu au clavier
        String line, name;
        boolean found = false;
        FileInputStream fis = getFIS();
        BufferedReader in = new BufferedReader(new InputStreamReader(fis));
        System.out.println("Veuillez saisir le nom de la personne a trouver");
        name = MyInOut.myReaderLine();
        System.out.println("Les informations retrouvees");
        for (int nbPerso = 0; (line = in.readLine()) != null; ++nbPerso) {
            String[] separated = line.split(",");
            if (separated[0].compareTo(name) == 0) {
                System.out.println("Fichier numero " + nbPerso);
                System.out.println("Nom :        " + separated[0]);
                System.out.println("Age :        " + separated[1]);
                System.out.println("Nb Enfants : " + separated[2] + "\n");
                found = true;
            }
        }
        if (!found) {
            System.out.println("La personne n'est pas trouvee");
        }
        fis.close();
    }

}
