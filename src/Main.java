import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        System.out.println("Test exercice 1");
        if (Fichier.exist("./out/fichier.txt")) {
            System.out.println("Fichier existe");
        } else {
            System.out.println("Fichier n'existe pas");
        }
        System.out.println();

        System.out.println(Fichier.countFile("out", "fichier.txt"));

        System.out.println("Le nom du fichier que vous voulez lire");
        System.out.print("Nom du fichier : ");
        String nameRepertory = MyInOut.myReaderLine();
        try {
            Repertory r = new Repertory(nameRepertory);
            r.inputInfo();
            r.lister();
            r.chercherInfo();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

}
