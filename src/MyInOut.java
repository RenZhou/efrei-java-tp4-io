import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MyInOut {
    public static String myReaderLine() {
        String str = "";
        while (str.isEmpty()) {
            try {
                InputStreamReader isr = new InputStreamReader(System.in);
                BufferedReader br = new BufferedReader(isr);
                str = br.readLine();

            } catch (IOException e) {
                System.out.println("erreur E/S");
            }
        }
        return str;
    }
}
