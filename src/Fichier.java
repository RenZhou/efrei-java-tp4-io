import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

/**
 * FileTool is a class which provides some methods used to work on Files and Folders.
 */
public class Fichier {

    /**
     * FileTools Constructor.
     */
    public Fichier() {
    }

    /**
     * Copy a file from a source to a destination.
     *
     * @param src Source file path.
     * @param dst Destination file path.
     */
    public static void copy(String src, String dst) {
        File source = new File(src);
        File destination = new File(dst);
        FileInputStream sourceFile;
        FileOutputStream destinationFile;
        try {
            destination.createNewFile();
            sourceFile = new FileInputStream(source);
            destinationFile = new java.io.FileOutputStream(destination);
            // Read by 0.5MB segment.
            byte buffer[] = new byte[512 * 1024];
            int nbRead;
            while ((nbRead = sourceFile.read(buffer)) != -1) {
                destinationFile.write(buffer, 0, nbRead);
            }
            sourceFile.close();
            destinationFile.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Copy all files and directories from a Folder to a destination Folder.
     * Must be called like: listAllFilesInFolder(srcFolderPath, "", srcFolderPath,
     * destFolderPath)
     *
     * @param currentFolder     Used for the recursive called.
     * @param relatedPath       Used for the recursive called.
     * @param sourceFolder      Source directory.
     * @param destinationFolder Destination directory.
     */
    public static void copyFolderToFolder(String currentFolder,
                                          String relatedPath, String sourceFolder, String destinationFolder) {
        // Current Directory.
        File current = new File(currentFolder);
        if (current.isDirectory()) {
            // List all files and folder in the current directory.
            File[] list = current.listFiles();
            if (list != null) {
                // Read the files list.
                for (File aList : list) {
                    // Create current source File
                    File tf = new File(sourceFolder + relatedPath + "\\"
                            + aList.getName());
                    // Create current destination File
                    File pf = new File(destinationFolder + relatedPath + "\\"
                            + aList.getName());
                    if (tf.isDirectory() && !pf.exists()) {
                        // If the file is a directory and does not exit in the
                        // destination Folder.
                        // Create the directory.
                        pf.mkdir();
                        copyFolderToFolder(tf.getAbsolutePath(), relatedPath
                                        + "\\" + tf.getName(), sourceFolder,
                                destinationFolder);
                    } else if (tf.isDirectory() && pf.exists()) {
                        // If the file is a directory and exits in the
                        // destination Folder.
                        copyFolderToFolder(tf.getAbsolutePath(), relatedPath
                                        + "\\" + tf.getName(), sourceFolder,
                                destinationFolder);
                    } else if (tf.isFile()) {
                        // If it is a file.
                        copy(sourceFolder + relatedPath + "\\"
                                + aList.getName(), destinationFolder
                                + relatedPath + "\\" + aList.getName());
                    }
                }
            }
        }
    }

    public static boolean exist(String filename) {
        return new File(filename).exists();
    }

    public static int countFile(String folder, String filename) {
        int nbFile = 0;
        File fileFolder = new File(folder);
        if (fileFolder.isDirectory()) {
            File[] list = fileFolder.listFiles();
            if (list != null) {
                for (File aList : list) {
                    nbFile += countFile(aList.getAbsolutePath(), filename);
                }
            }
        } else {
            if (fileFolder.getName().compareTo(filename) == 0) {
                nbFile++;
            }
        }
        return nbFile;
    }

}